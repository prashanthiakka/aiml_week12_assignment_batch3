# -*- coding: utf-8 -*-
"""6664_week12

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1kpUNyWqGQhKQwmpl8Fly87ulxpA3u18W
"""

def check_leap_year(year):
    if year % 400 == 0:
        return True
    elif year % 100 == 0:
        return False
    elif year % 4 == 0:
        return True
    else:
        return False

def validate_date(date_str):
    delimiters = ['/', '-', '.', '|']
    for delimiter in delimiters:
        if delimiter in date_str:
            day, month, year = map(int, date_str.split(delimiter))
            break
    else:
        raise ValueError("Invalid date format. Please use one of the following delimiters: / - . |")
    if month < 1 or month > 12:
        raise ValueError("Invalid month. Month should be between 1 and 12.")

    if day < 1 or day > 31:
        raise ValueError("Invalid day. Day should be between 1 and 31.")

    if month == 2 and day > 28:
      if not check_leap_year(year) or (check_leap_year(year) and day > 29):
            raise ValueError("Invalid day for February in a non-leap year.")

    if month in [4, 6, 9, 11] and day > 30:
        raise ValueError("Invalid day for the given month.")

    return day, month, year

def get_next_date(date_str):
    day, month, year = validate_date(date_str)
    max_days = 31

    if month == 2:
        max_days = 28
        if check_leap_year(year):
            max_days = 29
    elif month in [4, 6, 9, 11]:
        max_days = 30

    if day == max_days:
        day = 1
        if month == 12:
            month = 1
            year+= 1
        else:
            month += 1
    else:
        day += 1

    return f"{day:02d}-{month:02d}-{year}"

def get_previous_date(date_str):
    day, month, year = validate_date(date_str)

    if day == 1:
        if month == 1:
           month = 12
           year -= 1
        else:
            month -= 1

        max_days = 31
        if month == 2:
            max_days = 28
            if check_leap_year(year):
                max_days = 29
        elif month in [4, 6, 9, 11]:
            max_days = 30

        day = max_days
    else:
        day -= 1
    return f"{day:02d}-{month:02d}-{year}"

def get_day_of_year(date_str):
    day, month, year = validate_date(date_str)
    days_in_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    if check_leap_year(year):
        days_in_month[1] = 29

    day_of_year = sum(days_in_month[:month - 1]) + day
    return day_of_year
# Example usage:
user_date = input("Enter a date (dd/mm/yyyy): ")
try:
    next_date = get_next_date(user_date)
    previous_date = get_previous_date(user_date)
    day_of_year = get_day_of_year(user_date)

    print("Next date:", next_date)
    print("Previous date:", previous_date)
    print("Day of the year:", day_of_year)

except ValueError as e:
  print("Error.",str(e))

from datetime import datetime, timedelta

def check_leap_year(year):
    if year % 400 == 0:
        return True
    elif year % 100 == 0:
        return False
    elif year % 4 == 0:
        return True
    else:
        return False
def validate_date(date_str):
    delimiters = ['/', '-', '.', '|']
    for delimiter in delimiters:
        if delimiter in date_str:
            day, month, year = map(int, date_str.split(delimiter))
            break
    else:
        raise ValueError("Invalid date format. Please use one of the following delimiters: / - . |")

    if month < 1 or month > 12:
        raise ValueError("Invalid month. Month should be between 1 and 12.")

    if day < 1 or day > 31:
        raise ValueError("Invalid day. Day should be between 1 and 31.")

    if month == 2 and day > 28:
        if not check_leap_year(year) or (check_leap_year(year) and day > 29):
            raise ValueError("Invalid day for February in a non-leap year.")

    if month in [4, 6, 9, 11] and day > 30:
        raise ValueError("Invalid day for the given month.")

    return day, month, year

def get_next_date(date_str):
    day, month, year = validate_date(date_str)
    date = datetime(year, month, day) + timedelta(days=1)
    return date.strftime("%d-%m-%Y")

def get_previous_date(date_str):
    day, month, year = validate_date(date_str)
    date = datetime(year, month, day) - timedelta(days=1)
    return date.strftime("%d-%m-%Y")

def get_day_of_year(date_str):
    day, month, year = validate_date(date_str)
    date = datetime(year, month, day)
    day_of_year = date.timetuple().tm_yday
    return day_of_year

def format_date(date_str):
    day, month, year = validate_date(date_str)
    date = datetime(year, month, day)
    day_suffix = 'th' if 11 <= day <= 13 else {1: 'st', 2: 'nd', 3: 'rd'}.get(day % 10, 'th')
    return date.strftime("%A, %d{} %B, %Y").format(day_suffix)

# Example usage:
user_date = input("Enter a date (dd/mm/yyyy): ")
try:
    print("Entered date:", format_date(user_date))
    next_date = get_next_date(user_date)
    previous_date = get_previous_date(user_date)
    day_of_year = get_day_of_year(user_date)

    print("Next date:", format_date(next_date))
    print("Previous date:", format_date(previous_date))
    print("Day of the year:", day_of_year)

except ValueError as e:
  print("Error:",str(e))